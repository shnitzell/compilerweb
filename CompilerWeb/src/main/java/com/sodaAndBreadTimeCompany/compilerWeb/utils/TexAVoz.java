package com.sodaAndBreadTimeCompany.compilerWeb.utils;


import com.sun.speech.freetts.audio.AudioPlayer;
import com.sun.speech.freetts.audio.NullAudioPlayer;
import com.sun.speech.freetts.audio.SingleFileAudioPlayer;
import javax.sound.sampled.AudioSystem;
import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import java.io.File;
import java.io.FileInputStream;
import javax.sound.sampled.AudioFileFormat;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author BRAYAN
 */
public class TexAVoz {
    
    private Voice voice=null;
    private final String ruta = "brayanAudio.wav";
  
    public TexAVoz() throws Exception{
    	System.out.println("En la ruta");
       VoiceManager voiceManager = VoiceManager.getInstance();
       System.out.println("Fijando voz");
       this.voice = voiceManager.getVoice("kevin16");
       System.out.println("allocate");
       this.voice.allocate();       
       System.out.println("instanciado");
    }
    //----
    
    public void speak(String text) throws Exception{ this.voice.speak(text); }
    //----
    
    public String crearArchivo(String text) throws Exception{
        
        javax.sound.sampled.AudioFileFormat.Type type = getAudioType(ruta);
        AudioPlayer audioPlayer = null;
          if(audioPlayer == null){
             audioPlayer = new NullAudioPlayer();
             audioPlayer = new SingleFileAudioPlayer(getBasename(ruta),type);
             System.out.println("audioPlayer "+audioPlayer);
             this.voice.setAudioPlayer(audioPlayer);
             this.voice.speak(text);
             audioPlayer.close();
          }
          
            File wav = new File(ruta); 
            FileInputStream ficheroStream = new FileInputStream(wav); 
            int filelen = (int)wav.length();
            byte buffer[] = new byte[filelen]; 
            ficheroStream.read(buffer);
               
        return  new sun.misc.BASE64Encoder().encode(buffer);
    }
    //----
    
    public void close() throws Exception{ this.voice.deallocate(); }
    //----
    public static javax.sound.sampled.AudioFileFormat.Type getAudioType(String file){
         javax.sound.sampled.AudioFileFormat.Type types[] = AudioSystem.getAudioFileTypes();
         String extension = getExtension(file);
        for (AudioFileFormat.Type type : types) {
            if (type.getExtension().equals(extension)) { return type;  }
        }
             
         return null;
    }
    //----
    
    public static String getExtension(String path){
           int index = path.lastIndexOf(".");
           if(index == -1){ return null; }
           else{ return path.substring(index + 1); }
    }
    //----
    
    public static String getBasename(String path){
            int index = path.lastIndexOf(".");
            if(index == -1){ return path; }
            else{ return path.substring(0, index); }
    }
    //----

}
