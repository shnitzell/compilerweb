package com.sodaAndBreadTimeCompany.compilerWeb.controllers;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sodaAndBreadTimeCompany.compilerWeb.paser.AnalizadorSintactico;

@Controller
public class ParserController implements
		org.springframework.web.servlet.mvc.Controller {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/parser.co")
	@ResponseBody
	public String parse(@RequestParam("parse") String locale, Model model) throws UnsupportedEncodingException {
		logger.info("Antes decode: "+locale);
		logger.info("Pasando eso a la vaina esa " + locale);
		java.io.InputStream archivoPrueba = new ByteArrayInputStream(
				locale.getBytes());
		AnalizadorSintactico a = new AnalizadorSintactico();
		String aux = a.main(archivoPrueba);
		if (aux == null) {
			aux = "No se encontro error, Compilación correcta";

		}
		System.out.println("Ejecutado!" + aux);
		return aux;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
