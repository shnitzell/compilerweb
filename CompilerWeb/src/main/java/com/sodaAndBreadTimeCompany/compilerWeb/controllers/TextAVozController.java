package com.sodaAndBreadTimeCompany.compilerWeb.controllers;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sodaAndBreadTimeCompany.compilerWeb.utils.TexAVoz;

/**
 *
 * @author BRAYAN
 */
@Controller
public class TextAVozController implements
org.springframework.web.servlet.mvc.Controller {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);
	
	@RequestMapping(method = RequestMethod.GET, value = "/textohabla.co")
	@ResponseBody
    public String processRequest(@RequestParam("voice") String request, Model model){
		     logger.info("lo de la voz");
            // response.setContentType("text/html;charset=UTF-8");
            // PrintWriter out = response.getWriter();
             try {          
               TexAVoz  tVoz = new TexAVoz();
               System.out.println("Antes de el error");
               String s = tVoz.crearArchivo(request);
               model.addAttribute("voz", s );
               return s;
                        
            }
            catch (Exception e){ 
            	
            }
            
			return "home";
    }

	@Override
	public ModelAndView handleRequest(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



 
}
