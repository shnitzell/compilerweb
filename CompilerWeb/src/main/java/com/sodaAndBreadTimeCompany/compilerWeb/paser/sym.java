
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Thu May 21 17:25:49 COT 2015
//----------------------------------------------------

package com.sodaAndBreadTimeCompany.compilerWeb.paser;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int AND = 26;
  public static final int PYCOMA = 13;
  public static final int MENOR = 12;
  public static final int ELSE = 23;
  public static final int OR = 27;
  public static final int INT = 20;
  public static final int IGUAL = 10;
  public static final int MENORIGUAL = 4;
  public static final int WHILE = 25;
  public static final int MASNUM = 8;
  public static final int PARIZQ = 15;
  public static final int PARDER = 16;
  public static final int IF = 22;
  public static final int PARA = 19;
  public static final int MENOSUNO = 6;
  public static final int DO = 24;
  public static final int LLAVEIZQ = 17;
  public static final int STRING = 28;
  public static final int COMA = 14;
  public static final int LLAVEDER = 18;
  public static final int FUNC = 31;
  public static final int MAYOR = 11;
  public static final int EOF = 0;
  public static final int DIFERENTE = 2;
  public static final int MASUNO = 9;
  public static final int MAYORIGUAL = 3;
  public static final int error = 1;
  public static final int BOOL = 30;
  public static final int NULL = 29;
  public static final int DIG = 32;
  public static final int VAR = 21;
  public static final int SIGNCN = 7;
  public static final int MENOSNUM = 5;
}

