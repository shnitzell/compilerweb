/* --------------------------Codigo de Usuario----------------------- */
package com.sodaAndBreadTimeCompany.compilerWeb.paser;
import java_cup.runtime.*;
import java.io.Reader;
      
%% //inicio de opciones
   
/* ------ Seccion de opciones y declaraciones de JFlex -------------- */  
   
/* 
    Cambiamos el nombre de la clase del analizador a Lexer
*/
%class AnalizadorLexico

/*
    Activar el contador de lineas, variable yyline
    Activar el contador de columna, variable yycolumn
*/
%line
%column
    
/* 
   Activamos la compatibilidad con Java CUP para analizadores
   sintacticos(parser)
*/
%cup
   
/*
    Declaraciones

    El codigo entre %{  y %} sera copiado integramente en el 
    analizador generado.
*/
%{
    /*  Generamos un java_cup.Symbol para guardar el tipo de token 
        encontrado */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Generamos un Symbol para el tipo de token encontrado 
       junto con su valor */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}
   

/*
    Macro declaraciones
  
    Declaramos expresiones regulares que despues usaremos en las
    reglas lexicas.
*/
   
Digito = 0|[1-9][0-9]*

Signo = '+' | '-'

Letras = [a-z] | [A-Z]

Var = '#' {Letras} ({Letras}|{Digito})*

/*  Un salto de linea es un \n, \r o \r\n dependiendo del SO   */
Salto = \r|\n|\r\n 
   
/* Espacio es un espacio en blanco, tabulador \t, salto de linea 
    o avance de pagina \f, normalmente son ignorados */
Espacio     = {Salto} | [ \t\f]
  

%% //fin de opciones
/* -------------------- Seccion de reglas lexicas ------------------ */
   
/*
   Esta seccion contiene expresiones regulares y acciones. 
   Las acciones son c�digo en Java que se ejecutara cuando se
   encuentre una entrada valida para la expresion regular correspondiente */
   
   /* YYINITIAL es el estado inicial del analizador lexico al escanear.
      Las expresiones regulares solo ser�n comparadas si se encuentra
      en ese estado inicial. Es decir, cada vez que se encuentra una 
      coincidencia el scanner vuelve al estado inicial. Por lo cual se ignoran
      estados intermedios.*/

"<="    {
            System.out.print(" <>");
            return symbol(sym.DIFERENTE);
            //return new Symbol(sym.DIFERENTE, yychar, yyline, yytext());
        }
"=>="    {
            System.out.print(" >=");
            return symbol(sym.MAYORIGUAL);  
            //return new Symbol(sym.MAYORIGUAL, yychar, yyline, yytext());
        }

"=<="    {
            System.out.print(" <=");
            return symbol(sym.MENORIGUAL); 
            //return new Symbol(sym.MENORIGUAL, yychar, yyline, yytext());
        }

"-="    {
            System.out.print(" -=");
            return symbol(sym.MENOSNUM);   
            //return new Symbol(sym.MENOSNUM, yychar, yyline, yytext());
        }

"--"    {
            System.out.print(" --");
            return symbol(sym.MENOSUNO);             
          //return new Symbol(sym.MENOSUNO, yychar, yyline, yytext());
        }

"+="    {
            System.out.print(" +=");
            return symbol(sym.MASNUM);  
            //return new Symbol(sym.MASNUM, yychar, yyline, yytext());
        }

"++"    {
            System.out.print(" ++");
            return symbol(sym.MASUNO);           
          //return new Symbol(sym.MASUNO, yychar, yyline, yytext());
        }

"=>"     {
            System.out.print(" =");
            return symbol(sym.IGUAL);           
          //return new Symbol(sym.IGUAL, yychar, yyline, yytext());
        }

">>"     {
            System.out.print(" >");
            return symbol(sym.MAYOR);  
            //return new Symbol(sym.MAYOR, yychar, yyline, yytext());
        }

"<<"     {
            System.out.print(" <");
            return symbol(sym.MENOR);
          //return new Symbol(sym.MENOR, yychar, yyline, yytext());
        }

";"     {
            System.out.print(";");
            return symbol(sym.PYCOMA);          
          //return new Symbol(sym.PYCOMA, yychar, yyline, yytext());
        }

","     {
            System.out.print(",");
            return symbol(sym.COMA);    
          //return new Symbol(sym.COMA, yychar, yyline, yytext());
        }

"("     {
            System.out.print(" (");
            return symbol(sym.PARIZQ);         

          //return new Symbol(sym.PARIZQ, yychar, yyline, yytext());
        }

")"     {
            System.out.print(" )");
            return symbol(sym.PARDER); 

         // return new Symbol(sym.PARDER, yychar, yyline, yytext());
        }

"{"     {
            System.out.print(" {\n");
            return symbol(sym.LLAVEIZQ);        
        //return new Symbol(sym.LLAVEIZQ, yychar, yyline, yytext());
        }

"}"     {
             System.out.print(" }\n");
            return symbol(sym.LLAVEDER);        
          //return new Symbol(sym.LLAVEDER, yychar, yyline, yytext());
        }
"<-"     {
             System.out.print(" <-\n");
            return symbol(sym.SIGNCN);        
          //return new Symbol(sym.LLAVEDER, yychar, yyline, yytext());
        }

"para"   {
              System.out.print("Para ");
            return symbol(sym.PARA);     
          //return new Symbol(sym.FOR, yychar, yyline, yytext());
        }

"sir"   {
              System.out.print("if");
            return symbol(sym.IF);     
          //return new Symbol(sym.FOR, yychar, yyline, yytext());
        }        

"deci"   {
              System.out.print("else");
            return symbol(sym.ELSE);     
          //return new Symbol(sym.FOR, yychar, yyline, yytext());
        }

"ira"   {
              System.out.print("do");
            return symbol(sym.DO);     
          //return new Symbol(sym.FOR, yychar, yyline, yytext());
        } 

"demomento"   {
              System.out.print("while");
            return symbol(sym.WHILE);     
          //return new Symbol(sym.FOR, yychar, yyline, yytext());
        }         

"junto"   {
               System.out.print(" and");
            return symbol(sym.AND);
          //return new Symbol(sym.INT, yychar, yyline, yytext());
        }        

"ooo"   {
               System.out.print(" or");
            return symbol(sym.OR);
          //return new Symbol(sym.INT, yychar, yyline, yytext());
        }

"stein"   {
               System.out.print(" int");
            return symbol(sym.INT);
          //return new Symbol(sym.INT, yychar, yyline, yytext());
        }
"dataFruit" {
               System.out.print(" String");
            return symbol(sym.STRING);
          //return new Symbol(sym.INT, yychar, yyline, yytext());
        }
"charizar"  {
               System.out.print(" null");
            return symbol(sym.NULL);
          //return new Symbol(sym.INT, yychar, yyline, yytext());
        }

"bolibear"  {
               System.out.print(" String");
            return symbol(sym.BOOL);
          //return new Symbol(sym.INT, yychar, yyline, yytext());
        }

"funcion"  {
               System.out.print(" String");
            return symbol(sym.FUNC);
          //return new Symbol(sym.INT, yychar, yyline, yytext());
        }
        

//{Var}    {return new Symbol(sym.ID, yychar, yyline, yytext());}
{Var}    {
            System.out.print(" "+yytext());   
            return symbol(sym.VAR, new String(yytext()));
            //return new Symbol(sym.VAR, yychar, yyline, yytext());
         }

{Digito} {
            System.out.print(" "+yytext());  
            return symbol(sym.DIG, new Integer(yytext()));  
            //return new Symbol(sym.NUMERO, yychar, yyline, new Integer(yytext()));
          }

{Espacio} {}

.     { System.out.println("Caracter ilegal: " + yytext()); }